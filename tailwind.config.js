/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/app/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "gradient-custom":
          "linear-gradient(to right, rgba(30,213,169,1)0%, rgba(1,180,228,1) 100%)",
        "hero-image": "url('/hero-image.jpg')",
        "gradient-purple":
          "linear-gradient(to right, rgba(31.5, 31.5, 94.5, 1) calc((50vw - 170px) - 340px), rgba(31.5, 31.5, 94.5, 0.84) 50%, rgba(31.5, 31.5, 94.5, 0.84) 100%)",
      },
    },
    gridTemplateColumns: {
      "auto-fill-200": "repeat(auto-fill, minmax(200px, 1fr))",
    },
  },
  plugins: [],
};
