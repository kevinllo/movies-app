# Movies App
## Requirements
Create a movie webapp using The MovieDatabase API that displays movies and TV shows, with the ability to add/remove favorites using the login provided by TMDB API.
- Add a main page that shows a movies' section and tv shows' section by default, the list should be ordered by popularity, but the user also filter by: certification (based in the us), genres (1 or more genre) an release year.
- Create a search page where you can search both TV shows and movies by title and using the same endpoint.
- Make a movie detail page where you detail all the information related to the movie (title, overview, genres, images, rating, etc) there should be a section with credits (cast and crew), a section with the reviews and a section showing similar movies. The user should have the ability to add that specific movie to favorites.
- Add a tv show detail page where you show all the information (title, overview, images, rating, etc) there should be a section with the credits (cast and crew), the reviews and a final section with similar Tv shows. The user should have the ability to add/remove that tv show to favorites
- Create a season detail page where you show all the episodes of that season and a short description about it.
- Add a person detail page where you show information about a specific person (name, birthdate, death, aka, gender, biography)
- Optionally you can make a login page where the user can login to his previously created TMDB account or simply have a button in your Navbar redirecting to TMDB login page.
- Make a profile page where you show main information about the user, his favorites movies and favorites tvshows
- Add a 404 page

## Tools
- NextJS, Tailwind, Jest
## Deployment
Vercel