/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  env: {
    apiKey: "4da5df0fe1bd2b987d43fde84d45fafa",
    appUrl: "https://movies-app-kevinllo.vercel.app/",
  },
  images: {
    domains: ["image.tmdb.org", "secure.gravatar.com"],
  },
};

module.exports = nextConfig;
