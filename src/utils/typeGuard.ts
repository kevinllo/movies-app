import ILastToAir from "@/interfaces/ILastToAir";
import IMovieDetails from "@/interfaces/IMovieDetails";
import IMovies from "@/interfaces/IMovies";
import ISeason from "@/interfaces/ISeason";
import ITvShows from "@/interfaces/ITvShows";
import ITvShowsDetails from "@/interfaces/ITvShowsDetails";

function isMovie(data: IMovies | ITvShows): data is IMovies {
    return (data as IMovies).title !== undefined;
}
function isMovieDetail(data: IMovieDetails | ITvShowsDetails): data is IMovieDetails {
    return (data as IMovieDetails).title !== undefined;
}

function isLastToAir(data: ILastToAir | ISeason): data is ILastToAir {
    return (data as ILastToAir).still_path !== undefined;
}


export { isMovie, isMovieDetail, isLastToAir };