function getAvatarUrl(avatarPath: string) {
    //we check out the two possible strings in avatarPath.
    const avatarPathLength = avatarPath.split('/');
    const LONG_URL_PATH = 6;
    const SHORT_URL_PATH = 2;
    let imageUrl = '';
    // if the avatar path includes a complete url we create a new one with a different domain.
    if (avatarPath && avatarPathLength.length === LONG_URL_PATH) {
        imageUrl = `https://secure.gravatar.com/avatar/${avatarPathLength[LONG_URL_PATH - 1]}`;

    }
    //if the avatar path only includes the image path we create the image with another domain.
    if (avatarPath && avatarPathLength.length === SHORT_URL_PATH) {
        imageUrl = `https://image.tmdb.org/t/p/w500//${avatarPathLength[SHORT_URL_PATH - 1]}`
    }
    return imageUrl;
}

export default getAvatarUrl;