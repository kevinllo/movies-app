import axios from 'axios';

export default async function getUser(sessionId: string) {
    const { data } = await axios.get(`https://api.themoviedb.org/3/account?api_key=${process.env.apiKey}&session_id=${sessionId}`);
    return data;
}