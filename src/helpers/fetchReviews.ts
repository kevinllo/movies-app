import ApiReponse from "@/interfaces/ApiReponse";
import IReview from "@/interfaces/IReview";

async function fetchReviews(type: 'movie' | 'tv', id: string) {
    const url = `https://api.themoviedb.org/3/${type}/${id}/reviews?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const data = await response.json() as Promise<ApiReponse<IReview>>;
    return data;
}

export default fetchReviews;