import ApiReponse from "@/interfaces/ApiReponse";
import IMovies from "@/interfaces/IMovies";
import ITvShows from "@/interfaces/ITvShows";

const getPopulars = async (popularType: 'tv' | 'movie'): Promise<ApiReponse<IMovies & ITvShows>> => {
    const url = `https://api.themoviedb.org/3/${popularType}/popular?api_key=${process.env.apiKey}`;
    const response = await fetch(url, { cache: 'no-store' });
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const data = await response.json();
    return data;
};

export default getPopulars;