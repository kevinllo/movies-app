import axios from 'axios';

export default async function deleteSession(sessionId: string) {
    const { data } = await axios.delete(`https://api.themoviedb.org/3/authentication/session?api_key=${process.env.apiKey}`, { data: { session_id: sessionId } });
    return data;

}  