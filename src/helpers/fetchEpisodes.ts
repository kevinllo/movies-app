import EpisodeResponse from "@/interfaces/IEpisodeResponse";
import IEpisode from "@/interfaces/IEpisodes";

async function fetchEpisodes(id: string, idSeason: string) {
    const url = `https://api.themoviedb.org/3/tv/${id}/season/${idSeason}?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const episodes = await response.json() as Promise<EpisodeResponse>;
    return episodes;
}

export default fetchEpisodes;