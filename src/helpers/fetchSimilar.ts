import ApiReponse from "@/interfaces/ApiReponse";
import IMovies from "@/interfaces/IMovies";
import ITvShows from "@/interfaces/ITvShows";

async function fetchSimilars(type: 'movie' | 'tv', id: string) {
    const url = `https://api.themoviedb.org/3/${type}/${id}/similar?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const data = await response.json() as Promise<ApiReponse<IMovies & ITvShows>>;
    return data;
}

export default fetchSimilars;