import IPerson from "@/interfaces/IPerson";

async function fetchPersonDetail(id: string) {
    const url = `https://api.themoviedb.org/3/person/${id}?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const data = await response.json() as Promise<IPerson>;
    return data;
}

export default fetchPersonDetail;