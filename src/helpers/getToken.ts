import axios from 'axios'

export default async function getToken() {
    const { data } = await axios.get(`https://api.themoviedb.org/3/authentication/token/new?api_key=${process.env.apiKey}`);
    return data;
}