import ICredits from "@/interfaces/ICredits";

async function fetchCredits(type: 'movie' | 'tv', id: string) {
    const url = `https://api.themoviedb.org/3/${type}/${id}/credits?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const data = await response.json() as Promise<ICredits>;
    return data;
}

export default fetchCredits;