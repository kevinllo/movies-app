import ITvShowsDetails from "@/interfaces/ITvShowsDetails";

async function fetchTvShowsDetail(id: string) {
    const url = `https://api.themoviedb.org/3/tv/${id}?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const movieDetails = await response.json() as Promise<ITvShowsDetails>;
    return movieDetails;
}

export default fetchTvShowsDetail;