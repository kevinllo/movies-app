import IMovieDetails from "@/interfaces/IMovieDetails";

async function getFetchMovieDetails(id: string) {
    const url = `https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.apiKey}`;
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
    const movieDetails = await response.json() as Promise<IMovieDetails>;
    return movieDetails;
}

export default getFetchMovieDetails;