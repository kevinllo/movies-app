import axios from "axios";

export default async function getSession(requestToken: string) {
    const { data } = await axios.post(
        `https://api.themoviedb.org/3/authentication/session/new?api_key=${process.env.apiKey}`,
        { request_token: requestToken }
    );
    console.log(data);
    return data;
}
