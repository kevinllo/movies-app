/* eslint-disable consistent-return */
import { useState, useEffect, useRef } from 'react';
import IUseFetchState from '../interfaces/IUseFetchState';

const useFetch = <T>(url: string, config?:{}) => {
  const [fetchState, setFetchState] = useState<IUseFetchState<T>>({
    state: 'idle',
    data: null,
    error: null,
  });
  const cancelRequest = useRef<boolean>(false);

  useEffect(() => {
    cancelRequest.current = false;
    const fetchData = async () => {
      try {
        setFetchState((oldValues) => ({
          ...oldValues,
          state: 'loading',
        }));
        const response = await fetch(url, config);
        if (!response.ok) {
          setFetchState({ data: null, state: 'error', error: new Error(response.statusText) });
          return;
        }
        const dataJson = await response.json();
        setFetchState({ data: dataJson, state: 'success', error: null });
      } catch (error) {
        setFetchState({
          data: null,
          state: 'error',
          error: error as Error,
        });
      }
    };
    fetchData();
    return () => {
      cancelRequest.current = true;
    };
  }, [url, config]);

  return fetchState;
};

export default useFetch;
