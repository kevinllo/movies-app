'use client'

import getToken from '@/helpers/getToken';
import React, { createContext, useContext, useState } from 'react';
import { useRouter } from 'next/navigation'
import getSession from '@/helpers/getSession';
import getUser from '@/helpers/getUser';
import { useLocalStorage } from '@/hooks/useLocalStorage';
import deleteSession from '@/helpers/deleteSession';

type AuthContextType = {
  login: () => Promise<void>;
  logout: () => Promise<void>;
  createSession: (requestToken: string) => Promise<void>;
};

const AuthContext = createContext<AuthContextType>({
  login: async () => { },
  logout: async () => { },
  createSession: async () => { }
});

export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const router = useRouter();

  const login = async () => {
    const data = await getToken();
    router.push(`https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=${process.env.appUrl}/approved`);
  };

  const createSession = async (requestToken: string) => {
    const { session_id: sessionId } = await getSession(requestToken);
    const userInformation = await getUser(sessionId);
    localStorage.setItem('auth', JSON.stringify({ sessionId, userInformation, isAuthenticated: true }));
  }

  const logout = async () => {
    const auth = JSON.parse(localStorage.getItem('auth') || '');
    console.log(auth.sessionId);
    if (auth.sessionId) {
      const { success } = await deleteSession(auth.sessionId);
      if (success) {
        localStorage.clear();
        router.push('/');
      }
    }
  };

  return (
    <AuthContext.Provider value={{ login, logout, createSession }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
