import fetchPersonDetail from '@/helpers/fetchPersonDetail'
import React from 'react'
import Image from 'next/image';
import PlaceHolder from '../../assets/placeholder-image.jpg';

async function Person({ params: { id } }: { params: { id: string } }) {
    const {
        biography,
        birthday,
        profile_path,
        name,
        gender,
        deathday,
        place_of_birth
    } = await fetchPersonDetail(id);

    return (
        <section className='max-w-7xl mx-auto bg-slate-100 h-full min-h-[calc(100vh-136px)]' >
            <div className='flex flex-col md:flex-row gap-2 md:gap-8 p-10'>
                <div className='flex flex-col'>
                    <div className='max-h-[450px] max-w-[300px]'>
                        <Image
                            src={profile_path ? `https://image.tmdb.org/t/p/w500/${profile_path}` : PlaceHolder}
                            alt={name}
                            width={500}
                            height={500}
                            className='w-full h-full object-cover rounded-lg'
                            priority={true}
                        />
                    </div>
                    <div className='mt-4'>
                        <p className='flex flex-row md:flex-col font-bold text-md md:text-lg'>Gender
                            <span className='font-medium ml-4 md:ml-0'> {gender ? 'Male' : 'Female'}
                            </span></p>
                        <p className='flex flex-row md:flex-col font-bold text-md md:text-lg'>Birthday
                            <span className='font-medium ml-4 md:ml-0'> {new Date(birthday).toDateString() || 'No available'}
                            </span></p>
                        <p className='flex flex-row md:flex-col font-bold text-md md:text-lg'>Birthday
                            <span className='font-medium ml-4 md:ml-0'> {deathday || 'No available'}
                            </span></p>
                        <p className='flex flex-row md:flex-col font-bold text-md md:text-lg'>From
                            <span className='font-medium ml-4 md:ml-0'> {place_of_birth || 'No available'}
                            </span></p>
                    </div>
                </div>
                <div className='flex-1'>
                    <h2 className='font-bold text-3xl'>{name}</h2>
                    <div>
                        <h3 className='text-xl font-bold'>Biography</h3>
                        <p>{biography || 'No available'}</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Person  