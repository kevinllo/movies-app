'use client'

import React from 'react'
import FilterTvShows from '../components/FilterTvShows'
import TvShow from '../components/TvShow'

function TvShows() {
    return (
        <div className='px-10 py-8  max-w-7xl mx-auto'>
            <h1 className='px-3 border-l-[6.5px] border-cyan-500 text-xl md:text-2xl font-bold uppercase'>ALL TV SHOWS</h1>
            <FilterTvShows />
            <TvShow />
        </div>
    )
}

export default TvShows