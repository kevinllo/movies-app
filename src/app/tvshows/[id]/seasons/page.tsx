import GoBackButton from '@/app/components/GoBackButton'
import Season from '@/app/components/Season'
import fetchTvShowsDetail from '@/helpers/fetchTvShowsDetail'
import React, { Suspense } from 'react'

async function Seasons({ params: { id } }: { params: { id: string } }) {
    const { seasons } = await fetchTvShowsDetail(id);
    return (
        <>
            <GoBackButton message='Go back to shows' />
            <Suspense fallback={<p>Loading  seasons ...</p>}>
                <section className='mb-10 max-w-7xl mx-auto'>
                    <h2 className='text-2xl font-bold underline decoration-cyan-500 mb-5 px-10'>All Seasons</h2>
                    {seasons.map((season) => (
                        <Season tvId={id} season={season} key={season.id} />
                    ))}
                </section>
            </Suspense>
        </>
    )
}

export default Seasons