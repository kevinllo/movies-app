import GoBackButton from '@/app/components/GoBackButton'
import fetchEpisodes from '@/helpers/fetchEpisodes'
import React, { Suspense } from 'react'
import Image from 'next/image';
import PlaceHolder from '../../../../assets/placeholder-image.jpg'
import { StarIcon } from '@heroicons/react/24/solid';

async function SeasonDetail({ params: { id, idSeason } }: { params: { id: string, idSeason: string } }) {
    const { episodes } = await fetchEpisodes(id, idSeason);
    return (
        <>
            <GoBackButton message='Go back to seasons' />
            <Suspense fallback={<p>Loading episodes ...</p>}>
                <section className='max-w-7xl mx-auto'>
                    <h2 className='text-2xl font-bold underline decoration-cyan-500 mb-5 px-10'>Episodes</h2>
                    <div className='mb-10 px-10'>
                        {episodes.map(({ id, name, still_path, overview, episode_number, vote_average }) => (
                            <div className='flex flex-col md:flex-row gap-2  shadow-md bg-slate-50 rounded-md mb-10' key={id}>
                                <div className='w-full h-[300px] md:w-[200px] md:h-[200px]'>
                                    <Image
                                        src={still_path ? `https://image.tmdb.org/t/p/w500/${still_path}` : PlaceHolder}
                                        alt={name}
                                        width={500}
                                        height={500}
                                        className='w-full h-full object-cover rounded-t-md md:rounded-s-lg'
                                        priority={true}
                                    />
                                </div>
                                <div className='flex-1 px-6 py-2'>
                                    <div className='flex gap-2 items-center'>
                                        <h2 className='text-xl font-bold'>Episode {episode_number}</h2>
                                        <p className='text-white inline-flex items-center gap-1 px-3 bg-black rounded-md'><StarIcon width={10} height={10} color='white' />{vote_average.toFixed(1)}</p>
                                    </div>
                                    <p className='text-bold text-md font-bold'>{name}</p>
                                    <p className='w-full h-full overflow-hidden text-ellipsis'>{overview || 'No available'}</p>
                                </div>
                            </div>
                        ))}
                    </div>
                </section>
            </Suspense>
        </>
    )
}

export default SeasonDetail