import React, { Suspense } from 'react'
import Credits from '@/app/components/Credits';
import Review from '@/app/components/Review';
import fetchTvShowsDetail from '@/helpers/fetchTvShowsDetail';
import SimilarContent from '@/app/components/SimilarContent';
import DetailsSection from '@/app/components/DetailsSection';
import Season from '@/app/components/Season';

export default async function TvShowsDetails({ params: { id } }: { params: { id: string } }) {
    const tvDetails = await fetchTvShowsDetail(id);
    return (
        <>
            <Suspense fallback={<p className='text-center'>Loading details...</p>}>
                <DetailsSection details={tvDetails} />
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading credits...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <Credits idCredit={id} typeCredit='tv' />
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading seasons ...</p>}>
                <section className='mb-10 max-w-7xl mx-auto'>
                    <h2 className='text-2xl font-bold underline decoration-cyan-500 mb-5 px-10'>Last Season</h2>
                    <Season season={tvDetails.last_episode_to_air} tvId={id} />
                </section>
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading reviews...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <Review idReview={id} typeReview='tv' />
            </Suspense >
            <Suspense fallback={<p className='text-center'>Loading slider...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <SimilarContent idSimilar={id} typeSimilar='tv' text='tv shows' />
            </Suspense>
        </>
    )
}

