/* eslint-disable react-hooks/exhaustive-deps */
'use client'
import React, { useState } from 'react';
import SearchContent from '../components/SearchContent';
import SearchBar from '../components/SearchBar';

function Search() {
    const [search, setSearch] = useState<string>();
    const updateSearch = (e: React.ChangeEvent<HTMLInputElement>) => setSearch(e.target.value);
    return (
        <section className='max-w-7xl mx-auto px-10 md:px-16 my-10'>
            <h1 className='font-bold text-2xl md:text-3xl mb-5'>Search your favorite movie or tv show.
            </h1>
            <SearchBar updateSearch={updateSearch} />
            <SearchContent search={search} />
        </section>
    )
}

export default Search