'use client';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from 'next/image';
import Slider from 'react-slick';
import CustomPrevArrow from "./CustomPrevArrow";
import CustomNextArrow from "./CustomNextArrow";
import IMovies from "@/interfaces/IMovies";
import ITvShows from "@/interfaces/ITvShows";
import { isMovie } from "@/utils/typeGuard";
import Link from "next/link";
import PlaceHolder from '../assets/placeholder-image.jpg';

function SliderComponent({ sliderData }: { sliderData: IMovies[] | ITvShows[] }) {
    const settings = {
        dots: true,
        infinite: true,
        speed: 700,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ],
    };

    return (
        <div className="relative mt-10">
            <Slider {...settings} className="mt-7 max-w-7xl mx-auto">
                {sliderData.map((slider) => (
                    <Link
                        href={isMovie(slider) ? `/movies/${slider.id}` : `/tvshows/${slider.id}`}
                        key={slider.id} >
                        <div className="w-full h-[375px]">
                            <Image
                                src={slider.poster_path ? `https://image.tmdb.org/t/p/w500/${slider.poster_path}` : PlaceHolder}
                                alt={isMovie(slider) ? slider.title : slider.name}
                                width={500}
                                height={500}
                                className="mx-auto object-cover w-full h-full"
                            />
                        </div>
                        <p className='text-center font-bold text-lg'>{isMovie(slider) ? slider.title : slider.name}</p>
                    </Link>
                ))}
            </Slider>
        </div>
    )
}

export default SliderComponent