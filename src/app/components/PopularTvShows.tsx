import getPopulars from '@/helpers/fetchPopular';
import ApiReponse from '@/interfaces/ApiReponse';
import ITvShows from '@/interfaces/ITvShows';
import SliderComponent from './SliderComponent';

async function PopularTvShows() {
    const { results: tvShows }: ApiReponse<ITvShows> = await getPopulars('tv');
    return <SliderComponent sliderData={tvShows}/>

}

export default PopularTvShows


