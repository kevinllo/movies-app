import { MagnifyingGlassIcon } from '@heroicons/react/24/solid'
import React from 'react'
function SearchBar({ updateSearch }: { updateSearch(e: React.ChangeEvent<HTMLInputElement>): void }) {
    return (
        <div className='flex'>
            <input placeholder='Search ...'
                className='flex-1 placeholder-slate-500 text-md md:text-xl focus:outline-none input:underline border-b-2 border-cyan-500'
                onChange={(e) => updateSearch(e)} />
            <MagnifyingGlassIcon width={20} height={20} />
        </div>
    )
}

export default SearchBar