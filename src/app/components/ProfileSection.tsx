
import { UserCircleIcon } from '@heroicons/react/24/solid'
import React, { useEffect, useState } from 'react'
import FavoriteMovies from './FavoriteMovies'
import FavoriteShows from './FavoriteShows'
import { useAuth } from '../context/auth'

function ProfileSection({ auth }: any) {
    const { logout } = useAuth();
    return (
        <section>
            <div>
                <div className="bg-gradient-custom h-[100px]">
                    <div className='max-w-7xl mx-auto px-10 flex items-center h-full'>
                        <div className="flex flex-1 items-center">
                            <UserCircleIcon width={60} height={60} />
                            <p className="font-bold text-slate-800 text-2xl">{auth.userInformation.username}</p>
                        </div>
                        <button className="px-4 p-1 bg-slate-800 text-white focus:outline-dashed rounded-md font-bold" onClick={logout}>Logout</button>
                    </div>
                </div>
                <div className="mb-10">
                    <FavoriteMovies auth={auth} />
                    <FavoriteShows auth={auth} />
                </div>
            </div>
        </section>
    )
}

export default ProfileSection