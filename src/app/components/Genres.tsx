import React, { SyntheticEvent, useState } from 'react'
import Button from './Button';
import useFetch from '@/hooks/useFetch';
import IGenres from '@/interfaces/IGenres';
import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';

function Genres({ genreType }: { genreType: 'movie' | 'tv' }) {

    const { state, data, error } = useFetch<IGenres>(`https://api.themoviedb.org/3/genre/${genreType}/list?api_key=${process.env.apiKey}`);
    const { queryParams, setQueryParams } = useQueryParams<IQueryParams>();
    const [selectedGenre, setSelectedGenre] = useState<string[]>(queryParams?.get('genres')?.split(',') || []);

    const handleActiveButton = (e: SyntheticEvent) => {
        let newGenre: string[] = [];
        const genreId = e.currentTarget.id;
        if (selectedGenre.includes(genreId)) {
            newGenre = selectedGenre.filter(genre => genre !== genreId);
            setSelectedGenre(newGenre);
            setQueryParams({ genres: newGenre.join(), page: '1' });
            return;
        }
        newGenre = [...selectedGenre, genreId];
        setSelectedGenre(newGenre);
        setQueryParams({ genres: newGenre.join(), page: '1' });
    }

    if (state === 'loading') return <p className='text-center text-xl'>Laoding...</p>
    if (error) return <p className='text-center text-xl'>There was an error ...</p>

    return (
        <>
            <h2 className='font-bold mt-3 uppercase'>Gernes</h2>
            <div className='max-w-8xl flex items-start overflow-x-auto'>

                {data?.genres?.map(genre => (
                    <Button genre={genre}
                        key={genre.id}
                        handleActiveButton={handleActiveButton}
                    />
                ))}
            </div>
        </>
    )
}

export default Genres