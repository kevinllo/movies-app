import React from 'react'
import useFetch from '@/hooks/useFetch';
import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import { ChangeEvent } from 'react';

function SelectCertification() {
    const { state, data, error } = useFetch<ICertification>(`https://api.themoviedb.org/3/certification/movie/list?api_key=${process.env.apiKey}`);
    const { queryParams, setQueryParams } = useQueryParams<IQueryParams>();
    const certificationParam = queryParams?.get('certification');

    const handleCertification = (e: ChangeEvent<HTMLSelectElement>) => {
        setQueryParams({ certification: e.target.value, page: '1' });
    }
    if (state === 'loading') return <p className='text-center font-bold'>Loading...</p>;
    if (error) return <p className='text-center font-bold'>We could not load certifications</p>;
    return (
        <div className='flex flex-col items-center'>
            <label htmlFor="certification" className="block mt-2 text-md font-bold text-black">Select a certification</label>
            <select name="certification" id="certification" onChange={handleCertification} className='text-sm rounded block w-[200px] p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500' defaultValue={certificationParam || ''}>
                <option defaultValue='' value=''>All</option>
                {
                    data?.certifications.US.map(({ certification, order }) => (
                        <option value={certification} key={order}>{certification}</option>
                    ))
                }
            </select>
        </div>
    )
}

export default SelectCertification