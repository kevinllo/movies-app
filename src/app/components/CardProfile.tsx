import React from 'react'
import Image from 'next/image'
import PlaceHolder from '../../app/assets/placeholder-image.jpg';
import Link from 'next/link'
import IPerson from '@/interfaces/IPerson'

function CardProfile({ dataType }: { dataType: IPerson }) {
    return (
        <Link href={`/person/${dataType.id}`}>
            <div key={dataType.id} className='bg-slate-200 rounded relative w-full h-[390px] mx-auto'>
                <div className='w-full h-[70%]'>
                    <Image
                        src={dataType.profile_path ? `https://image.tmdb.org/t/p/w500/${dataType.profile_path}` : PlaceHolder}
                        alt={dataType.name}
                        width={500}
                        height={500}
                        className='w-full h-full object-cover rounded-t-[4px]'
                    />
                </div>
                <div className="px-4 mt-4">
                    <h2 className="text-[16px] font-semibold text-center">{dataType.name}</h2>
                </div>
            </div>
        </Link>
    )
}

export default CardProfile