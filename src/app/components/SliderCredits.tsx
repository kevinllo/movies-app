'use client';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from 'next/image';
import Slider from 'react-slick';
import CustomPrevArrow from "./CustomPrevArrow";
import CustomNextArrow from "./CustomNextArrow";
import ICast from "@/interfaces/ICast";
import PlaceHolder from '../assets/placeholder-image.jpg';
import ICrew from "@/interfaces/ICrew";
import Link from "next/link";

function SliderCredits({ credit, textCredit }: { credit: ICast[] | ICrew[], textCredit: string }) {
    const showSlider = credit.length > 4;
    const startIndex = 0;
    const endIndex = 20;
    const settings = {
        dots: true,
        infinite: true,
        speed: 700,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ],
    };
    return (
        <div className="relative mt-5">
            <h3 className='text-xl font-bold border-l-[5px] px-[5px] border-cyan-500'>{textCredit}</h3>
            {!showSlider ? <p className="text-xl font-bold text-center">There are not any {textCredit}</p> :
                <div className="mt-5">
                    <Slider {...settings}>
                        {credit.slice(startIndex, endIndex).map(({ id, profile_path, name }) => (
                            <Link href={`/person/${id}`} key={id}>
                                <div className="w-full h-[300px]">
                                    <Image
                                        src={profile_path ? `https://image.tmdb.org/t/p/w500/${profile_path}` : PlaceHolder}
                                        alt={name}
                                        width={500}
                                        height={500}
                                        className="mx-auto object-cover w-full h-full"
                                    />
                                </div>
                                <p className='text-center font-bold text-lg'>{name}</p>
                            </Link>
                        ))}
                    </Slider>
                </div>
            }
        </div>
    )
}

export default SliderCredits