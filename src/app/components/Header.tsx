'use client'
import React, { useEffect, useState } from 'react'
import { Bars3Icon, UserCircleIcon, XMarkIcon } from '@heroicons/react/24/solid'
import NavItems from './NavItems';
import { useAuth } from '../context/auth';
import Link from 'next/link'
import { useLocalStorage } from '@/hooks/useLocalStorage';


function Header() {
  const { login, logout } = useAuth();
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <header className='py-3 bg-[rgb(2,36,65)] text-white relative'>
      <div className="px-10 lg:px-none max-w-8xl mx-auto flex gap-4 items-center ">
        <div className="text-xl font-medium flex-1 md:flex-none">WIZARD TV</div>

        <nav className='hidden md:block md:flex-1'>
          <ul className='flex gap-4 font-semibold'>
            <NavItems />
          </ul>
        </nav>
        <div className={isMenuOpen ? 'absolute md:hidden min-h-screen inset-0 bg-cyan-900/80 z-10' : 'hidden'}>
          <nav className='flex items-center justify-center h-full'>
            <ul className='flex flex-col items-center justify-center gap-4 font-semibold text-3xl'>
              <NavItems />
            </ul>
          </nav>
        </div>
        <button className='text-sm font-bold text-slate-100 rounded-full px-5 py-1 bg-gradient-custom hover:scale-105'
          onClick={login}
        >
          Login
        </button>
        <button className='md:hidden z-20' onClick={toggleMenu}>
          {!isMenuOpen ? <Bars3Icon className='text-blue-50 h-7 w-7 ' /> : <XMarkIcon className='text-blue-50 h-7 w-7' />}
        </button>
      </div>
    </header>
  )
}

export default Header
