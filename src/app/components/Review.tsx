import React from 'react';
import fetchReviews from '@/helpers/fetchReviews';
import PlaceHolder from '../assets/placeholder-image.jpg';
import Image from 'next/image';
import getAvatarUrl from '@/utils/getAvatarUrl';

async function Review({ typeReview, idReview }: { typeReview: 'movie' | 'tv', idReview: string }) {
    const reviews = await fetchReviews(typeReview, idReview);

    return (
        <section className='max-w-7xl mx-auto px-10 mt-6 mb-14'>
            <h2 className='font-bold text-2xl underline decoration-cyan-500'>Reviews</h2>
            <div className='max-w-4xl mx-auto max-h-[500px] overflow-y-auto'>
                {reviews.results.length === 0 && <p className='text-xl font-bold text-center'>There are not any reviews yet</p>}
                {reviews.results.map(({ id, author_details, author, content }) => (
                    <div className='flex gap-4 bg-slate-200 mt-4 p-4 rounded-md' key={id}>
                        <div className='w-[40px] h-[40px] rounded-full'>
                            <Image
                                src={author_details.avatar_path ?
                                    getAvatarUrl(author_details.avatar_path)
                                    : PlaceHolder}
                                alt={author_details.name}
                                width={500}
                                height={500}
                                className="mx-auto object-cover w-full h-full rounded-full"
                            />
                        </div>
                        <div className='flex-1 w-full h-full'>
                            <p className='font-bold'>@{author}</p>
                            <p className='max-h-[75px] overflow-hidden'>{content}</p>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    )
}

export default Review