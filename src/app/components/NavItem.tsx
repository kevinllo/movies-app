import Link from 'next/link'
import React from 'react'

type propsT = {
    path: string,
    name: string,
    isActive: boolean
}

function NavItem({ path, name, isActive }: propsT) {
    return (
        <Link href={path}
            className={isActive ? 'border-solid bg-gradient-custom rounded px-2' : 'hover:text-zinc-300'}>
            {name}
        </Link>
    )
}

export default NavItem