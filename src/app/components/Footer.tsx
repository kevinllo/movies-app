import React from 'react'

function Footer() {
    return (
        <footer className="bg-[rgb(2,36,65)] text-white py-3">
            <div className="max-w-6xl mx-auto">
                <p className="text-center text-gray-200 text-md font-semibold">
                    © 2023 Applaudo Studios, Inc. All rights reserved.
                </p>
                <div className="flex justify-center mt-2">
                    <p className="text-gray-200 text-md">
                        Developed by <span className='text-cyan-200 font-semibold'>Kevin Grande</span>
                    </p>
                </div>
            </div>
        </footer>

    )
}

export default Footer