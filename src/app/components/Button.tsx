'use client'

import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import React, { SyntheticEvent } from 'react'

type propsButton = {
    genre: { id: number, name: string };
    handleActiveButton: (e: SyntheticEvent) => void;
}

function Button({ genre: { id, name }, handleActiveButton }: propsButton) {
    const { queryParams } = useQueryParams<IQueryParams>();
    const currentParams = queryParams?.get('genres')?.split(',');
    return (
        <button type='button'
            className='px-3 pb-1 pt-1 text-sm font-bold rounded-full border-2 border-cyan-500 border-solid mb-2 mr-4 whitespace-nowrap'
            style={{ backgroundColor: currentParams?.includes(id.toString()) ? '#0FC4C7' : '' }}
            id={id.toString()}
            onClick={handleActiveButton}> {name}</button>
    )
}


export default Button