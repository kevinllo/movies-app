'use client';

import { ArrowLeftIcon } from "@heroicons/react/24/solid";

function CustomPrevArrow({ onClick }: any) {
    return (
        <div className="absolute right-14 -top-[40px]" onClick={onClick}>
            <ArrowLeftIcon className="bg-gradient-custom text-slate-700 h-[30px] w-[30px] rounded-full grid place-items-center cursor-pointer" />
        </div>
    );
};

export default CustomPrevArrow;