import React from 'react'
import IMovies from '@/interfaces/IMovies'
import ITvShows from '@/interfaces/ITvShows'
import Card from './Card';

function Grid({ data }: { data: (IMovies | ITvShows)[] | undefined }) {
    return (
        <section className='mt-5 grid grid-cols-auto-fill-200 gap-4'>
            {data?.map((movie: IMovies | ITvShows) => (
                <Card key={movie.id} dataType={movie} />
            ))}
        </section>
    )
}

export default Grid