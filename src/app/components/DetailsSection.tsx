import React from 'react'
import Image from 'next/image';
import PlaceHolder from '../assets/placeholder-image.jpg'
import { isMovieDetail } from '@/utils/typeGuard';
import IMovieDetails from '@/interfaces/IMovieDetails';
import ITvShowsDetails from '@/interfaces/ITvShowsDetails';

function DetailsSection({ details }: { details: IMovieDetails | ITvShowsDetails }) {
    return (
        <section
            style={{
                backgroundImage: `url('https://image.tmdb.org/t/p/w500/${details.backdrop_path}')`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                marginBottom: '10px'
            }}>
            <div className='bg-gradient-purple'>
                <div className='max-w-7xl mx-auto p-10'>
                    <div className='flex flex-col md:flex-row gap-2 md:gap-8'>
                        <div className='flex-1 max-h-[450px] max-w-[300px]'>
                            <Image
                                src={details.poster_path ? `https://image.tmdb.org/t/p/w500/${details.poster_path}` : PlaceHolder}
                                alt={isMovieDetail(details) ? details.title : details.name}
                                width={500}
                                height={500}
                                className='w-full h-full object-cover'
                                priority={true}
                            />
                        </div>
                        <div className='flex-1'>
                            <h2 className='font-bold text-2xl md:text-3xl text-slate-100'>{isMovieDetail(details) ? details.title : details.name}</h2>
                            <p className='text-slate-100'>{new Date(isMovieDetail(details) ? details.release_date : details.first_air_date).toDateString()}</p>
                            <div className='inline-block mt-1 mb-4'>
                                {details.genres?.map(({ id, name }) => (<p key={id} className='bg-gradient-custom px-4 py-1 mr-2 mb-2 inline-block rounded-full font-bold text-slate-800 text-sm'>{name}</p>))}
                            </div>
                            <h3 className='text-slate-300 text-xl font-bold'>Overview</h3>
                            <p className='font-medium text-slate-100'>{details.overview}</p>
                            <div className='mt-2 w-[60px] h-[60px] rounded-full border-solid border-4 border-green-400 flex items-center justify-center'>
                                <span className='text-slate-100 relative font-bold text-[1.3rem]'>{Math.floor(details.vote_average * 10)}
                                    <span className='absolute left-[25px] bottom-[18px] text-[9.5px] font-bold'>%</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default DetailsSection