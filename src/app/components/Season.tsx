
import ISeason from '@/interfaces/ISeason'
import React from 'react'
import Image from 'next/image'
import PlaceHolder from '../assets/placeholder-image.jpg'
import ILastToAir from '@/interfaces/ILastToAir'
import { isLastToAir } from '@/utils/typeGuard'
import Link from 'next/link'

function Season({ season, tvId }: { season: ILastToAir | ISeason, tvId: string }) {
    const imagePath = isLastToAir(season) ? season.still_path : season.poster_path;
    return (
        <section className='max-w-7xl mx-auto px-10 mb-10'>
            <Link href={`/tvshows/${tvId}/seasons/${season.season_number}`}>
                <div className='flex flex-col md:flex-row gap-2  shadow-md bg-slate-50 rounded-md'>
                    <div className='w-full h-[300px] md:w-[200px] md:h-[200px]'>
                        <Image
                            src={imagePath ? `https://image.tmdb.org/t/p/w500/${imagePath}` : PlaceHolder}
                            alt={season.name}
                            width={500}
                            height={500}
                            className='w-full h-full object-cover rounded-t-md md:rounded-s-lg'
                            priority={true}
                        />
                    </div>
                    <div className='flex-1 px-6 py-2'>
                        <h2 className='text-xl font-bold'>Season {season.season_number}</h2>
                        <p className='text-sm font-medium'>{new Date(season.air_date).toDateString()}{` | ${isLastToAir(season) ? season.episode_number : season.episode_count} episodes`}</p>
                        <p className='w-full h-full overflow-hidden text-ellipsis'>{season.overview}</p>
                    </div>
                </div>
            </Link>
            {isLastToAir(season) && <Link href={`/tvshows/${tvId}/seasons`} className='text-cyan-900 hover:underline font-bold'>View all seasons</Link>}
        </section>
    )
}

export default Season