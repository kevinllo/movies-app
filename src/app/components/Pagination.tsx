/* eslint-disable react/no-array-index-key */
'use client';

import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/react/24/solid';

interface IFilterItems {
    currentPage: string | undefined;
    paginationRange: (string | number)[];
}

function Pagination({ currentPage, paginationRange }: IFilterItems) {
    const { setQueryParams } = useQueryParams<IQueryParams>();
    const canGoNext = Number(currentPage) === paginationRange[paginationRange.length - 1];
    const canGoBack = currentPage === '1';

    const handleClick = (page: number | string) => {
        if (page === currentPage || page === '...') return;
        setQueryParams({ page: page.toString() })
    };

    const onPrevious = () => {
        const newPage = (Number(currentPage) - 1).toString();
        setQueryParams({ page: newPage.toString() })
    };

    const onNext = () => {
        const newPage = (Number(currentPage) + 1).toString();
        setQueryParams({ page: newPage.toString() })
    };

    return (
        <section className="max-w-[700px] w-full mx-auto flex sm:justify-center my-4 overflow-x-auto gap-2">
            <button type="button" onClick={onPrevious} className="min-w-[40px] min-h-[40px] bg-gray-700 rounded-full inline-flex items-center justify-center disabled:bg-gray-300 disabled:pointer-events-none" disabled={canGoBack}>
                <ArrowLeftIcon className='text-white h-[20px] w-[20px] rounded-full cursor-pointer text-center' />
            </button>
            {
                paginationRange?.map((page: string | number, index: number) => (
                    <button
                        type="button"
                        key={index}
                        onClick={() => handleClick(page)}
                        style={{ pointerEvents: page === '...' ? 'none' : 'auto' }}
                        className={`min-w-[40px] min-h-[40px] rounded-full inline-flex justify-center items-center ${page.toString() === currentPage ? 'bg-gradient-custom font-bold' : 'bg-gray-200'
                            }`}
                    >
                        {page}
                    </button>
                ))
            }
            <button type="button" onClick={onNext} className="min-w-[40px] min-h-[40px] text-center bg-gray-700 rounded-full inline-flex items-center justify-center disabled:bg-gray-300 disabled:pointer-events-none" disabled={canGoNext}>
                <ArrowRightIcon className='text-white h-[20px] w-[20px] rounded-full cursor-pointer' />
            </button>
        </section>
    );
}

export default Pagination;
