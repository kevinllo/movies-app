'use client'
import useFetch from '@/hooks/useFetch';
import ApiReponse from '@/interfaces/ApiReponse';
import React from 'react'
import Grid from './Grid';
import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import Pagination from './Pagination';
import usePagination from '@/hooks/usePagination';
import ITvShows from '@/interfaces/ITvShows';

function TvShow() {
    const { queryParams } = useQueryParams<IQueryParams>();
    const pageParam = queryParams?.get('page');
    const genres = queryParams?.get('genres') ? `&with_genres=${queryParams.get('genres')}` : '';
    const dateYear = queryParams?.get('date_year') ? `&first_air_date_year=${queryParams.get('date_year')}` : ''
    const page = pageParam ? `&page=${pageParam}` : '&page=1';
    const { state, data: tvShows, error } = useFetch<ApiReponse<ITvShows>>(`https://api.themoviedb.org/3/discover/tv?api_key=${process.env.apiKey}${genres}${page}${dateYear}`);
    const paginationRange = usePagination(
        {
            totalCount: tvShows?.total_results || 0,
            pageSize: 20,
            currentPage: Number(pageParam) || 1,
        },
    );

    if (state === 'loading') return <p className='text-center font-bold'>Loading....</p>
    if (error) return <p className='text-center font-bold'>There was an error</p>
    if (tvShows?.results.length === 0) return <p className='text-center font-bold'>We could not find any tv shows</p>
    return (
        <>
            <Grid data={tvShows?.results} />
            {tvShows && tvShows.total_pages > 1 &&
                <Pagination
                    currentPage={pageParam || '1'}
                    paginationRange={paginationRange || []}
                />}
        </>
    )
}

export default TvShow