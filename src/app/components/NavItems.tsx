import React from 'react'
import NavItem from './NavItem'
import { usePathname } from 'next/navigation';
import { ROUTES } from '@/constants/routes';

function NavItems() {
    const pathname = usePathname();
    const isActive = (currentPath: string) => pathname?.split('/').pop() === currentPath.substring(1);
    return (
        <>
            {ROUTES.map(({ id, path, name }) => (
                <NavItem key={id} path={path} name={name} isActive={isActive(path)} />
            ))}
        </>
    )
}

export default NavItems