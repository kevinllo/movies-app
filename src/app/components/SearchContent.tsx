/* eslint-disable react-hooks/exhaustive-deps */
'use client'

import useDebounce from '@/hooks/useDebounce';
import useFetch from '@/hooks/useFetch'
import useQueryParams from '@/hooks/useQueryParams';
import ApiReponse from '@/interfaces/ApiReponse';
import IMovies from '@/interfaces/IMovies';
import IQueryParams from '@/interfaces/IQueryParams';
import ITvShows from '@/interfaces/ITvShows';
import React, { useEffect } from 'react'
import Grid from '../components/Grid';
import usePagination from '@/hooks/usePagination';
import Pagination from '../components/Pagination';
import GridSearchContent from './GridSearchContent';
import IPerson from '@/interfaces/IPerson';

function SearchContent({ search }: { search: string | undefined }) {
    const { queryParams, setQueryParams } = useQueryParams<IQueryParams>();
    const pageParam = queryParams?.get('page') || '1'
    const query = useDebounce(search, 500);
    const { state, data, error } = useFetch<ApiReponse<IMovies | ITvShows | IPerson>>(`https://api.themoviedb.org/3/search/multi?query=${query || ''}&page=${pageParam}&api_key=${process.env.apiKey}`);
    const paginationRange = usePagination(
        {
            totalCount: data?.total_results || 0,
            pageSize: 20,
            currentPage: Number(pageParam) || 1,
        },
    );

    useEffect(() => {
        if (query) {
            setQueryParams({ search: query, page: '1' });
            return;
        }
        if (query === '') {
            setQueryParams({ search: '', page: '1' });
            return;
        }
    }, [query])

    if (state === 'loading') return <p className='text-center font-bold text-xl'>Loading ...</p>;
    if (error) return <p className='text-center font-bold text-xl'>There was an error</p>;
    if (data?.results.length === 0) return <p className='text-center py-5 text-xl'>There are not results <span className='font-bold'>with {query || '...'}</span></p>;
    return (
        <>
            <GridSearchContent data={data?.results} />
            {data && data.total_pages > 1 &&
                <Pagination
                    currentPage={pageParam}
                    paginationRange={paginationRange} />
            }
        </>
    )
}

export default SearchContent