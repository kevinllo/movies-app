'use client'

import React from 'react'
import SelectCertification from './SelectCertification'
import SelectDateYear from './SelectDateYear'
import Genres from './Genres'

function FilterMovie() {
    return (
        <>
            <Genres genreType='movie' />
            <div className='flex gap-1 items-start flex-col min-[510px]:flex-row min-[510px]:gap-4'>
                <SelectCertification />
                <SelectDateYear />
            </div>
        </>
    )
}

export default FilterMovie