'use client'

import ArrowLeftIcon from '@heroicons/react/24/solid/ArrowLeftIcon'
import { usePathname, useRouter } from 'next/navigation';
import React from 'react'

function GoBackButton({ message }: { message: string }) {
    const router = useRouter();
    const currentPathName = usePathname();
    const splitPathName = currentPathName?.split('/');
    const lastElementUrl = splitPathName?.pop();
    const newRoute = splitPathName?.filter((path) => path !== lastElementUrl).join('/') || '';
    return (
        <div className='px-10 py-4 flex items-center gap-2 cursor-pointer'>
            <ArrowLeftIcon
                width={30}
                height={30}
                color='black'
                className='p-1 rounded-full bg-gradient-custom'
                onClick={() => router.replace(newRoute)}
            />
            <p className='text-lg font-bold'>{message}</p>
        </div>
    )
}

export default GoBackButton