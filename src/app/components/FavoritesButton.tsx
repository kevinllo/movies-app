'use client'

import useFetch from '@/hooks/useFetch'
import IAccountStates from '@/interfaces/IAccountStates';
import { BookmarkIcon } from '@heroicons/react/24/solid'
import { usePathname } from 'next/navigation'
import React from 'react'
import axios from 'axios';

function FavoritesButton({ typeFavorites, auth }: { typeFavorites: 'tv' | 'movie', auth: any }) {
    const currentPath = usePathname();
    const id = currentPath?.split('/')[2];
    const { state, data: favorites, error } = useFetch<IAccountStates>(`https://api.themoviedb.org/3/${typeFavorites}/${id}/account_states?api_key=${process.env.apiKey}&session_id=${auth.sessionId}`);

    const addToFavorites = async () => {
        const { data } = await axios.post(`https://api.themoviedb.org/3/account/${id}/favorite?api_key=${process.env.apiKey}&session_id=${auth.sessionId}`, { media_id: id, favorite: favorites?.favorite, media_type: typeFavorites });
        if(data.success){
            console.log('success');
        }

    }

    if (state === 'loading') return <p className='text-white'>Loading...</p>
    return (
        <div className='mb-2' onClick={addToFavorites}>
            <BookmarkIcon width={30} height={30} style={{
                color: favorites?.favorite ? 'cyan' : 'white'
            }} />
        </div>
    )
}

export default FavoritesButton