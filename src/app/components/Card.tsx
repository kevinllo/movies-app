import React from 'react'
import IMovies from '@/interfaces/IMovies'
import ITvShows from '@/interfaces/ITvShows'
import { isMovie } from '@/utils/typeGuard'
import Image from 'next/image'
import PlaceHolder from '../../app/assets/placeholder-image.jpg';
import Link from 'next/link'

function Card({ dataType }: { dataType: IMovies | ITvShows }) {
    const linkPath = isMovie(dataType) ? `/movies/${dataType.id}` : `/tvshows/${dataType.id}`;
    return (
        <Link href={linkPath}>
            <div key={dataType.id} className='bg-slate-200 rounded relative w-full h-[390px] mx-auto'>
                <div className='w-full h-[70%]'>
                    <Image
                        src={dataType.poster_path ? `https://image.tmdb.org/t/p/w500/${dataType.poster_path}` : PlaceHolder}
                        alt={isMovie(dataType) ? dataType.title : dataType.name}
                        width={500}
                        height={500}
                        className='w-full h-full object-cover rounded-t-[4px]'
                        priority={true}
                    />
                </div>

                <div className='absolute left-0 bottom-[98px] px-2'>
                    <div className='w-[35px] h-[35px] rounded-full text-white border-solid border-2 border-cyan-500 bg-[#022441] text-sm relative font-bold flex items-center justify-center'>
                        {Math.floor(dataType.vote_average * 10)}
                        <span className='absolute top-[-1.5px] right-[2.5px] text-[8px] font-bold'>%</span>
                    </div>
                </div>
                <div className="px-4 mt-4">
                    <h2 className="text-[16px] font-semibold text-center">{isMovie(dataType) ? dataType.title : dataType.name}</h2>
                    <p className='text-center text-[15px] text-ellipsis'>
                        {isMovie(dataType) ?
                            new Date(dataType.release_date).toDateString() :
                            new Date(dataType.first_air_date).toDateString()}
                    </p>
                </div>
            </div>
        </Link>
    )
}

export default Card