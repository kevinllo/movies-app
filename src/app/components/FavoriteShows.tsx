import useFetch from '@/hooks/useFetch';
import ApiReponse from '@/interfaces/ApiReponse';
import IMovies from '@/interfaces/IMovies';
import React from 'react'
import Grid from './Grid';

function FavoriteShows({ auth }: { auth: any }) {
    const { state, data, error } = useFetch<ApiReponse<IMovies>>(`https://api.themoviedb.org/3/account/${auth.userInformation.id}/favorite/tv?api_key=${process.env.apiKey}&session_id=${auth.sessionId}`);


    if (state === 'loading') return <p className="text-center font-bold tex-xl">Loading...</p>
    if (error) return <p className="text-center font-bold tex-xl">There was an error with your request</p>
    if (data?.results.length === 0) return <p className='font-bold text-xl'>There are not favorites shows so far</p>
    return (
        <div className="max-w-7xl mx-auto px-10">
            <h2 className="font-bold text-2xl uppercase mt-10">Favorites Tv Shows</h2>
            <Grid data={data?.results} />
        </div>
    )
}

export default FavoriteShows