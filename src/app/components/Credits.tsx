import fetchCredits from '@/helpers/fetchCredits'
import React from 'react'
import SliderCredits from './SliderCredits';

async function Credits({ idCredit, typeCredit }: { idCredit: string, typeCredit: 'movie' | 'tv' }) {
    const { cast, crew } = await fetchCredits(typeCredit, idCredit);
    return (
        <>
            <section className="mt-6 mb-14 max-w-7xl mx-auto px-10">
                <h2 className='text-2xl font-bold underline decoration-cyan-500'>Credits</h2>
                <SliderCredits credit={cast} textCredit='Cast' />
                <SliderCredits credit={crew} textCredit='Crew' />
            </section>

        </>
    )
}

export default Credits