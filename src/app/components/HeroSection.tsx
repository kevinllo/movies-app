import React from 'react'

function HeroSection() {
    return (
        <div className="relative bg-cover bg-center h-[350px] bg-hero-image">
            <div className="absolute inset-0"></div>
            <div className="absolute inset-0 flex items-center">
                <div className="px-10">
                    <h1 className="text-3xl sm:text-4xl md:text-5xl font-bold text-white">Welcome.</h1>
                    <p className="mt-2 text-gray-100 text-2xl sm:text-3xl md:text-4xl font-semibold">Millions of movies, TV shows and people to discover.</p>
                    <button className='mt-2 bg-gradient-custom px-4 py-1.5 rounded-full text-whi
                     font-bold text-slate-100 hover:scale-105 border-solid border-red-100'>Explore Now</button>
                </div>
            </div>
        </div>
    );
};

export default HeroSection