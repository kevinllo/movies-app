'use client'
import useFetch from '@/hooks/useFetch';
import ApiReponse from '@/interfaces/ApiReponse';
import IMovies from '@/interfaces/IMovies';
import React from 'react'
import Grid from './Grid';
import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import Pagination from './Pagination';
import usePagination from '@/hooks/usePagination';

function Movie() {
    const { queryParams } = useQueryParams<IQueryParams>();
    const pageParam = queryParams?.get('page');
    const certifications = queryParams?.get('certification') ? `&certification_country=US&certification=${queryParams.get('certification')}` : '';
    const genres = queryParams?.get('genres') ? `&with_genres=${queryParams.get('genres')}` : '';
    const dateYear = queryParams?.get('date_year') ? `&primary_release_year=${queryParams.get('date_year')}` : ''
    const page = pageParam ? `&page=${pageParam}` : '&page=1';
    const { state, data: movies, error } = useFetch<ApiReponse<IMovies>>(`https://api.themoviedb.org/3/discover/movie?api_key=${process.env.apiKey}${genres}${certifications}${page}${dateYear}`);
    const paginationRange = usePagination(
        {
            totalCount: movies?.total_results || 0,
            pageSize: 20,
            currentPage: Number(pageParam) || 1,
        },
    );

    if (state === 'loading') return <p className='text-center font-bold'>Loading....</p>
    if (error) return <p className='text-center font-bold'>There was an error</p>
    if (movies?.results.length === 0) return <p className='text-center font-bold'>We could not find any movie</p>
    return (
        <>
            <Grid data={movies?.results} />
            {movies && movies.total_pages > 1 &&
                <Pagination
                    currentPage={pageParam || '1'}
                    paginationRange={paginationRange || []}
                />}
        </>
    )
}

export default Movie