'use client'

import React from 'react'
import SelectDateYear from './SelectDateYear'
import Genres from './Genres'

function FilterTvShows() {
    return (
        <>
            <Genres genreType='tv' />
            <div className='flex gap-1 items-start flex-col min-[510px]:flex-row min-[510px]:gap-4'>
                <SelectDateYear />
            </div>
        </>
    )
}

export default FilterTvShows