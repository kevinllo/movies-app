import getPopulars from '@/helpers/fetchPopular';
import ApiReponse from '@/interfaces/ApiReponse';
import IMovies from '@/interfaces/IMovies';
import SliderComponent from './SliderComponent';

async function PopularMovies() {
    const { results: movies }: ApiReponse<IMovies> = await getPopulars('movie');
    return <SliderComponent sliderData={movies} />

}

export default PopularMovies


