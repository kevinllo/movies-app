'use client';

import { ArrowRightIcon } from "@heroicons/react/24/solid";

function CustomNextArrow({ onClick }: any) {
    return (
        <div className="absolute right-0 -top-[40px]" onClick={onClick}>
            <ArrowRightIcon className="bg-gradient-custom text-slate-700 h-[30px] w-[30px] rounded-full grid place-items-center cursor-pointer" />
        </div>
    );
};

export default CustomNextArrow;