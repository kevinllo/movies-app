import React from 'react'
import SliderComponent from './SliderComponent';
import fetchSimilars from '@/helpers/fetchSimilar';

async function SimilarContent({ idSimilar, typeSimilar, text }: { idSimilar: string, typeSimilar: 'movie' | 'tv', text: string }) {
    const { results: movies } = await fetchSimilars(typeSimilar, idSimilar);
    return (
        <section className='max-w-7xl mx-auto px-10 my-10'>
            <h2 className='text-2xl font-bold underline decoration-cyan-500 capitalize'>Similar {text}</h2>
            {movies.length === 0 && <p className='text-xl font-bold text-center'>There are not similar {text}</p>}
            <SliderComponent sliderData={movies} />
        </section>
    )
}

export default SimilarContent