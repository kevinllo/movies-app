import DATE_YEARS from '@/constants/dateyears';
import useQueryParams from '@/hooks/useQueryParams';
import IQueryParams from '@/interfaces/IQueryParams';
import React, { ChangeEvent } from 'react'

function SelectDateYear() {
    const { queryParams, setQueryParams } = useQueryParams<IQueryParams>();
    const dateYearParam = queryParams?.get('date_year');

    const handleDateYear = (e: ChangeEvent<HTMLSelectElement>) => {
        setQueryParams({ date_year: e.target.value, page: '1' });
    }
    return (
        <div className='flex flex-col items-center'>
            <label htmlFor="dateYear" className="mt-2 text-md font-bold text-black">Select a year</label>
            <select name="dateYear"
                id="dateYear"
                defaultValue={dateYearParam || ''}
                onChange={handleDateYear}
                className='text-sm rounded block w-[200px] p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500'
            >
                <option defaultValue='' value=''>All</option>
                {
                    DATE_YEARS.map((year) => (
                        <option value={year} key={year}>{year}</option>
                    ))
                }
            </select>
        </div>
    )
}

export default SelectDateYear