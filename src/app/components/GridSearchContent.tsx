import IMovies from '@/interfaces/IMovies'
import IPerson from '@/interfaces/IPerson'
import ITvShows from '@/interfaces/ITvShows'
import React from 'react'
import Card from './Card'
import CardProfile from './CardProfile'

function GridSearchContent({ data }: { data: (IMovies | ITvShows | IPerson)[] | undefined }) {
    return (
        <section className='mt-10 grid grid-cols-auto-fill-200 gap-4'>
            {data?.map((dataType: IMovies | ITvShows | IPerson) => {
                if ('profile_path' in dataType) return <CardProfile dataType={dataType} key={dataType.id} />
                return <Card dataType={dataType} key={dataType.id} />
            })}
        </section>
    )
}

export default GridSearchContent