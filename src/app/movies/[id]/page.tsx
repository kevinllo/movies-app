import React, { Suspense } from 'react'
import getFetchMovieDetails from '@/helpers/fetchMovieDetail'
import Image from 'next/image';
import PlaceHolder from '../../assets/placeholder-image.jpg';
import Credits from '@/app/components/Credits';
import Review from '@/app/components/Review';
import SimilarContent from '@/app/components/SimilarContent';
import DetailsSection from '@/app/components/DetailsSection';

export default async function MovieDetails({ params: { id } }: { params: { id: string } }) {
    const movieDetails = await getFetchMovieDetails(id);
    return (
        <>
            <Suspense fallback={<p className='text-center'>Loading details...</p>}>
                <DetailsSection details={movieDetails} />
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading credits...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <Credits idCredit={id} typeCredit='movie' />
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading reviews...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <Review idReview={id} typeReview='movie' />
            </Suspense>
            <Suspense fallback={<p className='text-center'>Loading slider...</p>}>
                {/* @ts-expect-error Async Server Component */}
                <SimilarContent idSimilar={id} typeSimilar='movie' text='movies' />
            </Suspense>
        </>
    )
}

