'use client'

import React from 'react'
import Movie from '../components/Movie'
import FilterMovie from '../components/FilterMovie'
function Movies() {
    return (
        <div className='px-10 py-8  max-w-7xl mx-auto'>
            <h1 className='px-3 border-l-[6.5px] border-cyan-500 text-xl md:text-2xl font-bold uppercase'>ALL MOVIES</h1>
            <FilterMovie />
            <Movie />
        </div>

    )
}

export default Movies