/* eslint-disable react-hooks/exhaustive-deps */
'use client'

import { useRouter } from 'next/navigation';
import React, { useEffect } from 'react'
import { useSearchParams } from 'next/navigation';
import { useAuth } from '../context/auth';

function Approved() {
    const router = useRouter();
    const { createSession } = useAuth();
    const searchParams = useSearchParams();
    const requestToken = searchParams?.get('request_token');
    const approved = searchParams?.get('approved');

    useEffect(() => {
        const getSessionId = async () => {  
            if (approved && requestToken) {
                await createSession(requestToken);
                router.push('/profile');
            } else {
                router.push('/');
            }
        }
        getSessionId();
    }, [approved, requestToken])
    return (
        <p className='text-center'>Loading ...</p>
    )
}

export default Approved