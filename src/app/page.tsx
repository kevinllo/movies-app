import { Suspense } from 'react';
import HeroSection from './components/HeroSection';
import PopularMovies from './components/PopularMovies';
import PopularTvShows from './components/PopularTvShows';

function Home(): JSX.Element {
  return (
    <>
      <HeroSection />
      <div className='px-10 py-8 max-w-7xl mx-auto'>
        <div>
          <h1 className='px-3 border-l-[6.5px] border-cyan-500 text-xl md:text-2xl font-bold uppercase'>Popular Movies</h1>
          <Suspense fallback={<div className='text-center'>Loading...</div>}>
            {/* @ts-expect-error Server Component */}
            <PopularMovies />
          </Suspense>
        </div>
        <div className='my-10'>
          <h1 className='px-3 border-l-[6.5px] border-cyan-500 text-xl md:text-2xl font-bold uppercase'>Popular TvShows</h1>
          <Suspense fallback={<div className='text-center'>Loading...</div>}>
            {/* @ts-expect-error Server Component */}
            <PopularTvShows />
          </Suspense>
        </div>
      </div>
    </>
  );
}

export default Home