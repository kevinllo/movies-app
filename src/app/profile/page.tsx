/* eslint-disable react-hooks/exhaustive-deps */
'use client'
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import ProfileSection from "../components/ProfileSection";

function Profile() {
    const router = useRouter();
    const [auth,] = useState(() => {
        try {
            const value = window.localStorage.getItem('auth')
            return value ? JSON.parse(value) : ''
        } catch {
            return null
        }
    });

    useEffect(() => {
        if (!auth) {
            router.push("/");
            return;
        }
    }, [auth]);
    if (!auth) {
        <p className="text-center text-lg mt-10">You must be logged in. Redirecting ...</p>
        return;
    } else {
        return <ProfileSection auth={auth} />
    }
}

export default Profile;
