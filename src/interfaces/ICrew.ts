interface ICrew {
    adult: boolean;
    gender: number;
    id: number;
    knownForDepartment: string;
    name: string;
    originalName: string;
    popularity: number;
    profile_path: string;
    creditId: string;
    department: string;
    job: string;
}

export default ICrew;