interface IAccountStates {
    id: number;
    favorite: boolean;
    rated: { value: number };
    watchlist: boolean;
}

export default IAccountStates;