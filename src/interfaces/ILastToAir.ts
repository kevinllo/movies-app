interface ILastToAir {
    id: number
    name: string
    overview: string
    vote_average: number
    vote_count: number
    air_date: string
    episode_number: number
    production_code: string
    runtime: any
    season_number: number
    show_id: number
    still_path: any
}

export default ILastToAir;