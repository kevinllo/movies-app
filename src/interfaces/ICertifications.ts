interface ICertification {
    certifications: {
        US: {
            certification: string;
            meaning: string;
            order: number;
        }[]
    }
}
