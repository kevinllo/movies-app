import IAuthorDetails from "./IAuthorDetails";

interface IReview {
    author: string;
    author_details: IAuthorDetails;
    content: string;
    created_at: string;
    id: string;
    updated_at: string;
    url: string;
}

export default IReview;