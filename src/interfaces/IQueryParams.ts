interface IQueryParams {
    genres: string;
    certification: string;
    date_year: string;
    page: string;
    search: string;
}

export default IQueryParams;