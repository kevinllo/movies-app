import IEpisode from "./IEpisodes"

interface EpisodeResponse {
    _id: string
    air_date: string
    episodes: IEpisode[]
    name: string
    overview: string
    id: number
    poster_path: string
    season_number: number
}

export default EpisodeResponse;