import IEpisode from "./ILastToAir"
import IGenres from "./IGenres"
import ISeason from "./ISeason"
interface Network {
    id: number
    logo_path: string
    name: string
    origin_country: string
}

interface ProductionCompany {
    id: number
    logo_path: any
    name: string
    origin_country: string
}

interface ProductionCountry {
    iso_3166_1: string
    name: string
}



interface SpokenLanguage {
    english_name: string
    iso_639_1: string
    name: string
}
interface ITvShowsDetails {
    adult: boolean
    backdrop_path: string
    created_by: any[]
    episode_run_time: number[]
    first_air_date: string
    genres: [{id:number;name:string}]
    homepage: string
    id: number
    in_production: boolean
    languages: string[]
    last_air_date: string
    last_episode_to_air: IEpisode
    name: string
    next_episode_to_air: any
    networks: Network[]
    number_of_episodes: number
    number_of_seasons: number
    origin_country: string[]
    original_language: string
    original_name: string
    overview: string
    popularity: number
    poster_path: string
    production_companies: ProductionCompany[]
    production_countries: ProductionCountry[]
    seasons: ISeason[]
    spoken_languages: SpokenLanguage[]
    status: string
    tagline: string
    type: string
    vote_average: number
    vote_count: number
}

export default ITvShowsDetails;