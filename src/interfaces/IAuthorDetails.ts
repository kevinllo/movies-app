interface IAuthorDetails {
    name: string;
    username: string;
    avatar_path: string;
    rating: number;
}

export default IAuthorDetails;