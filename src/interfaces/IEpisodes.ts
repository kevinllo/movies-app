export interface IEpisode {
    air_date: string
    episode_number: number
    id: number
    name: string
    overview: string
    production_code: string
    runtime: any
    season_number: number
    show_id: number
    still_path: any
    vote_average: number
    vote_count: number
    crew: any[]
    guest_stars: any[]
}

export default IEpisode;