interface ICast {
    adult: boolean;
    gender: number;
    id: number;
    knownForDepartment: string;
    name: string;
    originalName: string;
    popularity: number;
    profile_path: string;
    castId: number;
    character: string;
    creditId: string;
    order: number;
  }

  export default ICast;