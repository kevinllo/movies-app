interface IGenres {
    genres: {
        id: number;
        name: string;
    }[]
}
export default IGenres;