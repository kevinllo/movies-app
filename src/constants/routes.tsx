export const ROUTES = [
    {
        id: 1,
        path: '/',
        name: 'Home'
    },
    {
        id: 2,
        path: '/movies',
        name: 'Movies'
    },
    {
        id: 3,
        path: '/tvshows',
        name: 'Tv Shows'
    },
    {
        id: 4,
        path: '/search',
        name: 'Search'
    }
]